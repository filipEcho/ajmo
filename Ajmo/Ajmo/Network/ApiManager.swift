//
//  ApiManager.swift
//  Ajmo
//
//  Created by Filip Stevanoski on 10/7/20.
//  Copyright © 2020 Filip Stevanoski. All rights reserved.
//

import Foundation
import Alamofire

typealias CompletitionCallBack = ((_ success: Bool, _ responseObject: [String:Any]?,_ statusCode : Int?)-> ())?

class ApiManager  {
    
    static let sharedInstance = ApiManager()
    
    //MARK: MAIN FUNCTION
    private func executeRequest(request : URLRequestConvertible,completitionCallback : CompletitionCallBack) {
        Alamofire.request(request).responseJSON { (response) in
            switch response.result {
            case .success(_):
                let json = response.result.value as? [String:Any]
                
                var statusCode = 200
                if let res = response.response {
                    statusCode = res.statusCode
                    if statusCode == 200 {
                        completitionCallback!(true,json,200)
                    } else {
                        completitionCallback!(true,nil,400)
                    }
                }
            case .failure(_):
                completitionCallback!(false,nil,nil)
            }
        }
    }
    
    //MARK: GET VENUES
    func getVenues(completitionCallback : CompletitionCallBack) {
        self.executeRequest(request: Router.GetVenues) { (success, response, statusCode) in
            completitionCallback!(success,response,statusCode)
        }
    }
}
