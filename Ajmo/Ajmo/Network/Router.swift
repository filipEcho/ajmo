//
//  Router.swift
//  Ajmo
//
//  Created by Filip Stevanoski on 10/7/20.
//  Copyright © 2020 Filip Stevanoski. All rights reserved.
//

import Foundation
import Alamofire

enum Router: URLRequestConvertible {
    
    case GetVenues
    
    var method: Alamofire.HTTPMethod {
        return .get
    }
    
    var path: String {
        switch self {
        case .GetVenues:
            return Constants.Network.Endpoints.venues
        }
    }
    
    var header : [String : String] {
        return [
            Constants.Network.accept : Constants.Network.applicationJson,
            Constants.Network.contentType : Constants.Network.applicationJson,
        ]
    }
    
    func asURLRequest() throws -> URLRequest {
        var urlRequest : URLRequest!
        let url = URL(string: Constants.Network.kBaseUrl + path)
        urlRequest = URLRequest(url: url!)
        
        urlRequest.httpMethod = method.rawValue
        urlRequest.allHTTPHeaderFields = header
        return urlRequest
    }
}
