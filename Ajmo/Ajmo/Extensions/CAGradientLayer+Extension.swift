//
//  CAGradientLayer+Extension.swift
//  Ajmo
//
//  Created by Filip Stevanoski on 10/9/20.
//  Copyright © 2020 Filip Stevanoski. All rights reserved.
//

import Foundation
import UIKit

//MARK: - CAGRADIANTLAYER EXTENSION
extension CAGradientLayer {
    
    class func primaryGradient(on view: UIView?) -> UIImage? {
        //Checking if the view exists
        guard let onView = view else { return nil }
        let gradient = CAGradientLayer()
        //Converting the hex codes from constants to UIColor
        let fromColor = Utilities.sharedInstance.colorFromHexCode(hex: Constants.Colors.navColorFrom)
        let toColor = Utilities.sharedInstance.colorFromHexCode(hex: Constants.Colors.navColorTo)
        var bounds = onView.bounds
        bounds.size.height += onView.window?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0
        gradient.frame = bounds
        gradient.colors = [fromColor.cgColor, toColor.cgColor]
        gradient.startPoint = CGPoint(x: 0, y: 0)
        gradient.endPoint = CGPoint(x: 1, y: 0)
        return gradient.createGradientImage(on: onView)
    }
    
    private func createGradientImage(on view: UIView) -> UIImage? {
        var gradientImage: UIImage?
        UIGraphicsBeginImageContext(view.frame.size)
        if let context = UIGraphicsGetCurrentContext() {
            render(in: context)
            gradientImage = UIGraphicsGetImageFromCurrentImageContext()?.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch)
        }
        UIGraphicsEndImageContext()
        return gradientImage
    }
}
