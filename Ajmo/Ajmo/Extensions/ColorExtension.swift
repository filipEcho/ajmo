//
//  ColorExtension.swift
//  Ajmo
//
//  Created by Filip Stevanoski on 10/8/20.
//  Copyright © 2020 Filip Stevanoski. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    var isLight: Bool {
        var white: CGFloat = 0
        getWhite(&white, alpha: nil)
        return white > 0.5
    }
}
