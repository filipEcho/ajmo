//
//  ControllerExtension.swift
//  Ajmo
//
//  Created by Filip Stevanoski on 10/9/20.
//  Copyright © 2020 Filip Stevanoski. All rights reserved.
//

import Foundation
import  UIKit

extension UIViewController {
    
    func setNavigationBar(title : String,largeTitle : Bool,displayModeAlways : Bool) {
        self.navigationItem.title = title
        self.navigationController?.navigationBar.prefersLargeTitles = largeTitle
        if displayModeAlways {
            self.navigationItem.largeTitleDisplayMode = .always
        } else {
            self.navigationItem.largeTitleDisplayMode = .never
        }
        
        self.navigationController?.navigationBar.tintColor = .white
        
        //Getting gradient image and set the image to the navigation bar
        if let flareGradientImage = CAGradientLayer.primaryGradient(on: navigationController?.navigationBar) {
            let navBarAppearance = UINavigationBarAppearance()
            navBarAppearance.configureWithOpaqueBackground()
            navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
            if largeTitle {
                navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
            }
            navBarAppearance.backgroundColor = UIColor(patternImage: flareGradientImage)
            self.navigationController?.navigationBar.standardAppearance = navBarAppearance
            self.navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
        }
    }
}
