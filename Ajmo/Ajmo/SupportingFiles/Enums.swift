//
//  Enums.swift
//  Ajmo
//
//  Created by Filip Stevanoski on 10/8/20.
//  Copyright © 2020 Filip Stevanoski. All rights reserved.
//

import Foundation

enum IphoneType {
    case IPhoneXorBigger
    case IPhonePlus
    case Iphone5
    case Other
}
