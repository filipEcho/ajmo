//
//  Theme.swift
//  Ajmo
//
//  Created by Filip Stevanoski on 10/7/20.
//  Copyright © 2020 Filip Stevanoski. All rights reserved.
//

import Foundation
import UIKit

class Theme {

    static let sharedInstance = Theme()
    
    func getBackgroundColor() -> UIColor {
        Utilities.sharedInstance.colorFromHexCode(hex: "#ECEBF2")
    }
    
}
