//
//  Utilities.swift
//  Ajmo
//
//  Created by Filip Stevanoski on 10/7/20.
//  Copyright © 2020 Filip Stevanoski. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class Utilities {
    
    static let sharedInstance = Utilities()
    
    
    //MARK: - GET COLOR FROM HEX CODE
    func colorFromHexCode(hex: String) -> UIColor {
        let r, g, b, a: CGFloat
        
        var hexColor = hex
        
        if hex.hasPrefix("#") {
            hexColor = hex.trimmingCharacters(in: .whitespacesAndNewlines)
            hexColor = hexColor.replacingOccurrences(of: "#", with: "")
        }
        
        if hexColor.count == 8 {
            let scanner = Scanner(string: hexColor)
            var hexNumber: UInt64 = 0
            
            if scanner.scanHexInt64(&hexNumber) {
                r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                a = CGFloat(hexNumber & 0x000000ff) / 255
                
                return UIColor(red: r, green: g, blue: b, alpha: a)
            }
        } else if (hexColor.count == 6) {
            let scanner = Scanner(string: hexColor)
            var hexNumber: UInt64 = 0
            
            if scanner.scanHexInt64(&hexNumber) {
                r = CGFloat((hexNumber & 0xFF0000) >> 16) / 255.0
                g = CGFloat((hexNumber & 0x00FF00) >> 8) / 255.0
                b = CGFloat(hexNumber & 0x0000FF) / 255.0
                
                return UIColor(red: r, green: g, blue: b, alpha: 1.0)
            }
        }
        return .lightGray
    }
    
    //MARK: - CHECK INTERNET CONNECTION
    func hasInternetConnection() -> Bool {
        return Reachability.sharedInstance.isInternetAvailable()
    }
    
    //MARK: - CHECK IPHONE TYPE
    func iphoneType(type: IphoneType) -> Bool {
        let bounds = UIScreen.main.bounds
        let maxLength = max(bounds.size.height, bounds.size.width)
        switch type {
        case .Iphone5:
            return (maxLength <= 568)
        case .Other:
            return (maxLength > 568) && (maxLength <= 667)
        case .IPhonePlus:
            return (maxLength > 667) && (maxLength <= 736)
        case .IPhoneXorBigger:
            return (maxLength > 736)
        }
    }
    
    //MARK: - CREATE LABEL
    func createLabelWith(text: String, txtAlignment: NSTextAlignment, font: UIFont, textColor: UIColor, backgroundColor: UIColor) -> UILabel {
        let label = UILabel()
        label.text = text
        label.font = font
        label.textColor = textColor
        label.backgroundColor = backgroundColor
        label.textAlignment = txtAlignment
        label.numberOfLines = 0
        
        return label
    }
    
    //MARK: - CREATE VIEWS
    func createView(cornerRadius: CGFloat, backgorundColor: UIColor) -> UIView {
        let view = UIView()
        view.backgroundColor = backgorundColor
        view.layer.cornerRadius = cornerRadius
        
        return view
    }
    
    //MARK: - CREATE BUTTON
    func createButtonWith(title: String, titleColor: UIColor, font: UIFont, txtAligment: NSTextAlignment, imageName: String?, corner: CGFloat, backgroundColor: UIColor, tag: Int) -> UIButton {
        let button = UIButton()
        button.setTitle(title, for: .normal)
        button.setTitleColor(titleColor, for: .normal)
        
        if let imageNameString = imageName {
            button.setImage(UIImage(named: imageNameString), for: .normal)
        }
        
        if (txtAligment == .right) {
            button.contentHorizontalAlignment = .right
        } else if (txtAligment == .left) {
            button.contentHorizontalAlignment = .left
        }
        button.titleLabel?.font = font
        button.layer.cornerRadius = corner
        button.backgroundColor = backgroundColor
        button.tag = tag
        
        return button
    }
    
    func createButtonWithImage(imageName: String, backgroundColor: UIColor, tag: Int) -> UIButton {
        let button = UIButton()
        if (imageName != "") {
            button.setImage(UIImage(named: imageName) , for: .normal)
        }
        button.imageView?.contentMode = .scaleAspectFit
        button.backgroundColor = backgroundColor
        button.tag = tag
        return button
    }
    
    //MARK: - CREATE TEXT FIELD
    func createTextfieldWith(placeHolder: String, backgroundColor: UIColor, textColor: UIColor, font: UIFont, txtAlignment: NSTextAlignment, secureEntry: Bool, returnKeyType: UIReturnKeyType, corner: CGFloat) -> UITextField {
        let textField = UITextField()
        textField.placeholder = placeHolder
        textField.textColor = textColor
        textField.backgroundColor = backgroundColor
        textField.font = font
        textField.textAlignment = txtAlignment
        
        textField.layer.sublayerTransform = CATransform3DMakeTranslation(15, 0, 0)
        textField.autocorrectionType = .no
        textField.returnKeyType = returnKeyType
        textField.isSecureTextEntry = secureEntry
        textField.layer.cornerRadius = corner
        
        return textField
    }
    
    //MARK: - CREATE IMAGE VIEW
    func createImageViewWith(imageName: String, backgroundColor: UIColor, contentMode: UIView.ContentMode, corner: CGFloat) -> UIImageView {
        let imageView = UIImageView()
        if (imageName != "") {
            imageView.image = UIImage(named: imageName)
        }
        imageView.backgroundColor = backgroundColor
        imageView.contentMode = contentMode
        imageView.layer.cornerRadius = corner
        imageView.clipsToBounds = true
        imageView.kf.indicatorType = .activity
        
        return imageView
    }
    
    //MARK: - ADD SHADOW
    func addShadowTo(view: UIView) {
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.15
        view.layer.shadowOffset = .zero
        view.layer.shadowRadius = 4
        view.layer.shadowPath = UIBezierPath(roundedRect: view.bounds, cornerRadius: view.layer.cornerRadius).cgPath
        view.layer.shouldRasterize = true
        view.layer.rasterizationScale = UIScreen.main.scale
    }
    
    //MARK:- CONVERT JSON TO DATA
    func jsonToData(json: Any) -> Data? {
        do {
            return try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted)
        } catch let myJSONError {
            print(myJSONError)
        }
        return nil
    }
    
    //MARK: GET THE CURRENT WEEK DAY & HOUR
    func getTheCurrentWeekDay() -> Int {
        return Calendar.current.component(.weekday, from: Date())
    }
    
    func getTheCurrentHour() -> Int {
        return Calendar.current.component(.hour, from: Date())
    }
    
   //MARK: - TIME STRING TO INT CONVERSION
    func convertTimeStringToInt(timeString: String) -> Int? {
        let components = timeString.split { $0 == ":" } .map { (x) -> Int in return Int(String(x))! }

        if components.count > 0 {
            if components[0] == 0 {
                return 24
            }
            return components[0]
        }
        
        return nil
    }
    
    //MARK: - IMAGE STRING TO URL
    func getUrlFromImageString(imageString: String?) -> URL? {
        if let image = imageString {
            if let imageUrl = URL(string: image) {
                return imageUrl
            }
        }
        return nil
    }
    
    //MARK: - SORTING AND FILTERING
    func sortVenuesAlphabetically(venues: [Venue]) -> [Venue] {
        return venues.sorted { $0.name?.lowercased() ?? "" < $1.name?.lowercased() ?? ""}
    }
    
    func filterVenuesByCategory(category: VenueCategory, venues: [Venue]) -> [Venue] {
        var filteredVenues = [Venue]()
        for venue in venues {
            for venueCategory in venue.venue_categories! {
                if venueCategory.id == category.id {
                    filteredVenues.append(venue)
                }
            }
        }
        return filteredVenues
    }
    
    func filterVenuesByPopularTag(tag: PopularTag, venues: [Venue]) -> [Venue] {
        var filteredVenues = [Venue]()
        for venue in venues {
            if venue.allTags?.primaryTags?.contains(where: { $0.id == tag.tag?.id }) ?? false {
                filteredVenues.append(venue)
            }
        }
        
        return filteredVenues
    }
    
    func filterVenuesByPrimaryTag(tag: Tag, venues: [Venue]) -> [Venue] {
        var filteredVenues = [Venue]()
        for venue in venues {
            if venue.allTags?.primaryTags?.contains(where: { $0.id == tag.id }) ?? false {
                filteredVenues.append(venue)
            }
        }
        
        return filteredVenues
    }
    
    func filterVenuesBySecondaryTag(tag: Tag, venues: [Venue]) -> [Venue] {
        var filteredVenues = [Venue]()
        for venue in venues {
            if venue.allTags?.secondaryTags?.contains(where: { $0.id == tag.id }) ?? false {
                filteredVenues.append(venue)
            }
        }
        
        return filteredVenues
    }
    
    ///Check if the Venue is Open
    func isVenueOpen(startString: String, endString: String) -> Bool {
        if let start = Utilities.sharedInstance.convertTimeStringToInt(timeString: startString) { // get the start hour
            if let end = Utilities.sharedInstance.convertTimeStringToInt(timeString: endString) { // get end hour
                if end < start {
                    if Utilities.sharedInstance.getTheCurrentHour() >= start {
                        return true
                    }
                } else {
                    if Utilities.sharedInstance.getTheCurrentHour() >= start && Utilities.sharedInstance.getTheCurrentHour() <= end { //compare with the current hour
                        return true
                    }
                }
            }
        }
        
        return false
    }
}
