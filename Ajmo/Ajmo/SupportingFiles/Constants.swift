//
//  Constants.swift
//  Ajmo
//
//  Created by Filip Stevanoski on 10/7/20.
//  Copyright © 2020 Filip Stevanoski. All rights reserved.
//

import Foundation

struct Constants {
    
    struct Network {
        
        static let kBaseUrl = "https://api.ajmo.hr/v3/"
        
        static let contentType = "Content-Type"
        static let applicationJson = "application/json"
        static let accept = "Accept"
        
        struct Endpoints {
            static let venues = "venue/dashboard"
        }
    }
    
    struct Colors {
        static let navColorFrom = "#F5B22E"
        static let navColorTo = "#F59A46"
    }
    
    struct Landing {
        static let kDiscover = "Discover"
        static let kVenues = "Venues"
    }
    
    struct VenueCategory {
        static let kPromotedCell = "promotedCell"
        static let kPopularTagsCell = "popularTagsCell"
        static let kAllVenuesCell = "allVenuesCell"
        static let kTrending = "Trending"
        static let kAllVenues = "All Venues"
        static let kPromotedCellString = "promotedCell"
        static let kTagsCell = "tagsCell"
        static let kHotPicks = "HOT PICK"
        static let kPromotedImage = "promoted"
        
    }
    
    struct Venue {
        
        struct Cells {
            static let kVenueNameCell = "venueNameCell"
            static let kVenueInfoCell = "venueInfoCell"
            static let kSmokingCell = "smokingCell"
            static let kDescrtiptionCell = "descriptionCell"
            static let kGalleryCell = "galleryCell"
            static let kVenuePrimaryTagsCell = "venuePrimaryTagsCell"
            static let kVenueSecondaryTagsCell = "secondaryTagsCell"
            static let kHoursCell = "hoursCell"
            static let kTagsCell = "tagsCell"
        }
        
        struct Images {
            static let kLocationImageName = "location"
            static let kPhoneImageName = "phone"
            static let kClockImageName = "clock"
            static let kSmokingImageName = "smoking-area"
            static let kNoSmokingImageName = "non-smoking-area"
        }
        
        struct Strings {
            static let kOK = "OK"
            static let kOpen = "Open"
            static let kClosed = "Closed"
            static let kSmoking = "Smoking area"
            static let kNoSmoking = "No Smoking area"
            static let kIdealPlace = "Ideal place for..."
            static let kWhatToExpect = "What to expect"
            
            struct Days {
                static let kMOn = "MON"
                static let kTue = "TUE"
                static let kWed = "WED"
                static let kThu = "THU"
                static let kFri = "FRI"
                static let KSat = "SAT"
                static let kSun = "SUN"
                
            }
        }
    }
    
    struct Gallery {
        static let kDone = "Done"
        
        struct Cells {
            static let kImagesCell = "imagesCell"
        }
    }
    
    struct FilteredVenues {
        static let kNoVenues = "No Venues"
        static let kFilters = "Filters: "
        
        struct Cells {
            static let kAllVenuesCell = "allVenuesCell"
        }
    }
}
