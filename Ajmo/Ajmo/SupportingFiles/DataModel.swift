//
//  DataModel.swift
//  Ajmo
//
//  Created by Filip Stevanoski on 10/8/20.
//  Copyright © 2020 Filip Stevanoski. All rights reserved.
//

import Foundation

import UIKit

class DataModel: NSObject {

    static let sharedInstace = DataModel()
    
    var venues = [Venue]()
}
