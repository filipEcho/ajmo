//
//  GalleryViewController.swift
//  Ajmo
//
//  Created by Filip Stevanoski on 10/8/20.
//  Copyright © 2020 Filip Stevanoski. All rights reserved.
//

import UIKit

class GalleryViewController: UIViewController {

    var imagesCollectionView: UICollectionView!
    var indexOfFirstImage: Int!
    var images = [Gallery]()
    
    //MARK: - INITIALIZATION
    init(images: [Gallery], indexOfFirstImage: Int) {
        super.init(nibName: nil, bundle: nil)
        self.images = images
        self.indexOfFirstImage = indexOfFirstImage
    }
        
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupViews()
        setupConstraints()
    }
    
    //MARK: - SETUP NAVIGATION BAR
    func setupNavigationBar() {
        self.setNavigationBar(title: "", largeTitle: false, displayModeAlways: true)
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: Constants.Gallery.kDone, style: .plain, target: self, action: #selector(doneButtonTapped))
    }
    
    func setupViews() {
        self.view.backgroundColor = .black
        self.edgesForExtendedLayout = []
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        
        imagesCollectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        imagesCollectionView.backgroundColor = .clear
        imagesCollectionView.register(VenueImageCollectionViewCell.self, forCellWithReuseIdentifier: Constants.Gallery.Cells.kImagesCell)
        imagesCollectionView.showsHorizontalScrollIndicator = false
        imagesCollectionView.isPagingEnabled = true
        imagesCollectionView.delegate = self
        imagesCollectionView.dataSource = self
        
        imagesCollectionView.performBatchUpdates(nil, completion: { result in //Scroll to the selected image
            self.imagesCollectionView.scrollToItem(at: IndexPath(item: self.indexOfFirstImage, section: 0), at: .right, animated: false)
        })
        
        view.addSubview(imagesCollectionView)
    }
    
    func setupConstraints() {
        imagesCollectionView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
    }
    
    //MARK: - BUTTON ACTIONS
    @objc func doneButtonTapped() {
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK: - UICOLLECTIONVIEW DELEGATE, DATASOURCE & FLOW LAYOUT
extension GalleryViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.Gallery.Cells.kImagesCell, for: indexPath) as! VenueImageCollectionViewCell
        cell.setupCellWit(image: images[indexPath.row])
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.size.width, height: collectionView.frame.size.height*0.95)
    }
}
