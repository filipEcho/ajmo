//
//  VenueCategoryViewController.swift
//  Ajmo
//
//  Created by Filip Stevanoski on 10/7/20.
//  Copyright © 2020 Filip Stevanoski. All rights reserved.
//

import UIKit

class VenueCategoryViewController: UIViewController {

    //Properties
    var tableView: UITableView!
    var venueCategory: VenueCategory!
    var trendingVenues = [Venue]()
    var promotedVenues = [Venue]()
    
    //MARK: - INITIALIZATION
    init(venueCategory: VenueCategory, trendingVenues: [Venue]) {
        super.init(nibName: nil, bundle: nil)
        self.venueCategory = venueCategory
        self.trendingVenues = trendingVenues
        self.promotedVenues = self.trendingVenues.filter() { $0.bat == 1}
        self.title = self.venueCategory.name ?? ""
    }
        
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - CONTROLLER LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupConstraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.title = self.venueCategory.name ?? ""
        self.navigationItem.largeTitleDisplayMode = .always
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }

    //MARK: - SETUP VIEWS AND CONSTRAINTS
    func setupViews() {
        self.view.backgroundColor = Theme.sharedInstance.getBackgroundColor()
        self.edgesForExtendedLayout = []
        
        //TableView
        tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = .clear
        tableView.tableFooterView = UIView()
        tableView.register(PromotedVenuesTableViewCell.self, forCellReuseIdentifier: Constants.VenueCategory.kPromotedCell)
        tableView.register(TrendingTagsTableViewCell.self, forCellReuseIdentifier: Constants.VenueCategory.kPopularTagsCell)
        tableView.register(VenueTableViewCell.self, forCellReuseIdentifier: Constants.VenueCategory.kAllVenuesCell)
        tableView.showsVerticalScrollIndicator = true
        
        view.addSubview(tableView)
    }
    
    func setupConstraints() {
        tableView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
    }
}

 //MARK: - TABLEVIEW DELEGATE AND DATASOURCE
extension VenueCategoryViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section < 2 {
            return 1
        } else {
            return trendingVenues.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            if promotedVenues.count > 0 {
                if Utilities.sharedInstance.iphoneType(type: .IPhoneXorBigger) {
                    return self.view.frame.size.height/1.9
                } else {
                    return self.view.frame.size.height/1.6
                }
            }
            return 0.0
        } else if indexPath.section == 1 {
            if Utilities.sharedInstance.iphoneType(type: .IPhoneXorBigger) {
                return self.view.frame.size.height/4.5
            } else {
                return self.view.frame.size.height/3.5
            }
        } else {
            if Utilities.sharedInstance.iphoneType(type: .IPhoneXorBigger) {
                return self.view.frame.size.height/6.5
            } else {
                return self.view.frame.size.height/5.5
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.VenueCategory.kPromotedCell, for: indexPath) as! PromotedVenuesTableViewCell
            cell.setupCellWith(promotedVenues: self.promotedVenues)
            cell.delegate = self
            
            return cell
        } else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.VenueCategory.kPopularTagsCell, for: indexPath) as! TrendingTagsTableViewCell
            cell.setupCellWith(popularTags: venueCategory.popularTags ?? [PopularTag]())
            cell.delegate = self
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.VenueCategory.kAllVenuesCell, for: indexPath) as! VenueTableViewCell
            let sortedVenues = Utilities.sharedInstance.sortVenuesAlphabetically(venues: trendingVenues)
            cell.setupCellWith(venue: sortedVenues[indexPath.row])
            cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: .greatestFiniteMagnitude)
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section > 0 {
            let view = UIView()
            view.backgroundColor = Theme.sharedInstance.getBackgroundColor()
            var sectionTitleLabel: UILabel!
            if section == 1 {
                sectionTitleLabel = Utilities.sharedInstance.createLabelWith(text: Constants.VenueCategory.kTrending, txtAlignment: .left, font: .systemFont(ofSize: 20, weight: .semibold), textColor: .secondaryLabel, backgroundColor: .clear)
            } else {
                sectionTitleLabel = Utilities.sharedInstance.createLabelWith(text: Constants.VenueCategory.kAllVenues, txtAlignment: .left, font: .systemFont(ofSize: 22, weight: .semibold), textColor: .black, backgroundColor: .clear)
            }
            view.addSubview(sectionTitleLabel)
            
            sectionTitleLabel.snp.makeConstraints { (make) in
                make.left.equalTo(view).offset(20)
                make.centerY.equalTo(view.snp.centerY)
            }
            
            return view
        }
        
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section > 0 {
            return self.view.frame.size.height/14
        }
        
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 2 {
            let sortedVenues = Utilities.sharedInstance.sortVenuesAlphabetically(venues: trendingVenues)
            self.navigationController?.pushViewController(VenueViewController(selectedVenue: sortedVenues[indexPath.row]), animated: true)
        }
    }
}

 //MARK: - TRENDING TAGS CELL DELEGATE
extension VenueCategoryViewController: TrendingTagsTableViewCellDelegate {
    func tagSelected(tag: PopularTag) {
        self.navigationController?.pushViewController(FilteredVenuesViewController(filteredVenues: Utilities.sharedInstance.sortVenuesAlphabetically(venues: Utilities.sharedInstance.filterVenuesByPopularTag(tag: tag, venues: self.trendingVenues)), tag: tag.tag), animated: true)
    }
}

 //MARK: - POPULAR VENUES CELL DELEGATE
extension VenueCategoryViewController: PromotedVenuesTableViewCellDelegate {
    func showPromotedVenue(venue: Venue) {
        self.navigationController?.pushViewController(VenueViewController(selectedVenue: venue), animated: true)
    }
}

//MARK: SECONDARY TAGS CELL DELEGATYE
extension VenueViewController : SecondaryTagsTableViewCellDelegate {
    func tagPressed(tag: Tag) {
        self.navigationController?.pushViewController(FilteredVenuesViewController(filteredVenues: Utilities.sharedInstance.filterVenuesBySecondaryTag(tag: tag, venues: DataModel.sharedInstace.venues), tag: tag), animated: true)
    }
}
