//
//  TrendingVenuesTableViewCell.swift
//  Ajmo
//
//  Created by Filip Stevanoski on 10/7/20.
//  Copyright © 2020 Filip Stevanoski. All rights reserved.
//

import UIKit
import CardsLayout

protocol PromotedVenuesTableViewCellDelegate {
    func showPromotedVenue(venue: Venue)
}

class PromotedVenuesTableViewCell: UITableViewCell {
    
    //Properties
    var promotedVenuesCollectionView: UICollectionView!
    var promotedVenues = [Venue]()
    
    var delegate: PromotedVenuesTableViewCellDelegate?

    //MARK:- INITIALIZATION
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- SETUP VIEWS AND CONSTRAINTS
    func setupViews() {
        contentView.backgroundColor = Theme.sharedInstance.getBackgroundColor()
        selectionStyle = .none
        
        let cardLayout = CardsCollectionViewLayout()
        //cardLayout.itemSize = CGSize(width: 500, height: 300)
        
        promotedVenuesCollectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: cardLayout)
        promotedVenuesCollectionView.backgroundColor = .clear
        promotedVenuesCollectionView.register(PromotedVenueCollectionViewCell.self, forCellWithReuseIdentifier: Constants.VenueCategory.kPromotedCellString)
        promotedVenuesCollectionView.showsHorizontalScrollIndicator = false
        promotedVenuesCollectionView.isPagingEnabled = true
        promotedVenuesCollectionView.delegate = self
        promotedVenuesCollectionView.dataSource = self
        
        contentView.addSubview(promotedVenuesCollectionView)
    }
    
    func setupConstraints() {
        promotedVenuesCollectionView.snp.makeConstraints { (make) in
            make.edges.equalTo(contentView)
        }
    }
    
    //MARK: - SETUP CELL
    func setupCellWith(promotedVenues: [Venue]) {
        self.promotedVenues = promotedVenues
    }
}

//MARK: - UICOLLECTIONVIEW DELEGATE, DATASOURCE & FLOW LAYOUT
extension PromotedVenuesTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.promotedVenues.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.VenueCategory.kPromotedCellString, for: indexPath) as! PromotedVenueCollectionViewCell
        cell.setupCellWith(promotedVenue: promotedVenues[indexPath.row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.showPromotedVenue(venue: promotedVenues[indexPath.row])
    }
}
