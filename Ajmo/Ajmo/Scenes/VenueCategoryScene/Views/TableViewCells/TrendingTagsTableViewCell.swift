//
//  TrendingTagsTableViewCell.swift
//  Ajmo
//
//  Created by Filip Stevanoski on 10/7/20.
//  Copyright © 2020 Filip Stevanoski. All rights reserved.
//

import UIKit

protocol TrendingTagsTableViewCellDelegate {
    func tagSelected(tag: PopularTag)
}

class TrendingTagsTableViewCell: UITableViewCell {
    
    //Properties
    var tagsCollectionView: UICollectionView!
    var tags = [PopularTag]()
    
    var delegate: TrendingTagsTableViewCellDelegate?

    //MARK:- INITIALIZATION
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- SETUP VIEWS AND CONSTRAINTS
    func setupViews() {
        contentView.backgroundColor = Theme.sharedInstance.getBackgroundColor()
        selectionStyle = .none
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 20
        layout.minimumInteritemSpacing = 5
        //layout.sectionInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)
        
        tagsCollectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        tagsCollectionView.backgroundColor = .clear
        tagsCollectionView.register(TagCollectionViewCell.self, forCellWithReuseIdentifier: Constants.VenueCategory.kTagsCell)
        tagsCollectionView.showsHorizontalScrollIndicator = false
        tagsCollectionView.delegate = self
        tagsCollectionView.dataSource = self
        
        contentView.addSubview(tagsCollectionView)
    }
    
    func setupConstraints() {
        tagsCollectionView.snp.makeConstraints { (make) in
            make.centerY.equalTo(contentView.snp.centerY)
            make.left.equalTo(contentView).offset(20)
            make.right.equalTo(contentView).offset(-20)
            make.height.equalTo(contentView.snp.height).multipliedBy(0.8)
        }
    }

    //MARK: - SETUP CELL
    func setupCellWith(popularTags: [PopularTag]) {
        self.tags = popularTags
    }
}

//MARK: - UICOLLECTIONVIEW DELEGATE, DATASOURCE & FLOW LAYOUT
extension TrendingTagsTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tags.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.VenueCategory.kTagsCell, for: indexPath) as! TagCollectionViewCell
        cell.setupCellWith(popularTag: tags[indexPath.row])
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (contentView.frame.size.width - 60)/2 , height: (tagsCollectionView.frame.size.height - 5)/2.05)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.tagSelected(tag: self.tags[indexPath.row])
    }
}
