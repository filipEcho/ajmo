//
//  VenueTableViewCell.swift
//  Ajmo
//
//  Created by Filip Stevanoski on 10/8/20.
//  Copyright © 2020 Filip Stevanoski. All rights reserved.
//

import UIKit

class VenueTableViewCell: UITableViewCell {
    
    //Properties
    var venueImageView: UIImageView!
    var venueNameLabel: UILabel!
    var venueCategoryLabel: UILabel!
    var venueAddressLabel: UILabel!
    var promotedView: UIView!
    var promotedImageView: UIImageView!

    //MARK:- INITIALIZATION
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- SETUP VIEWS AND CONSTRAINTS
    func setupViews() {
        contentView.backgroundColor = Theme.sharedInstance.getBackgroundColor()
        selectionStyle = .none
        
        venueImageView = Utilities.sharedInstance.createImageViewWith(imageName: "", backgroundColor: .clear, contentMode: .scaleAspectFill, corner: 5.0)
        venueNameLabel = Utilities.sharedInstance.createLabelWith(text: "", txtAlignment: .left, font: .systemFont(ofSize: 13, weight: .bold), textColor: .black, backgroundColor: .clear)
        venueNameLabel.numberOfLines = 1
        venueCategoryLabel = Utilities.sharedInstance.createLabelWith(text: "", txtAlignment: .left, font: .systemFont(ofSize: 12, weight: .regular), textColor: .secondaryLabel, backgroundColor: .clear)
        venueCategoryLabel.numberOfLines = 1
        venueAddressLabel = Utilities.sharedInstance.createLabelWith(text: "", txtAlignment: .left, font: .systemFont(ofSize: 11, weight: .regular), textColor: .secondaryLabel, backgroundColor: .clear)
        venueAddressLabel.numberOfLines = 1
        
        promotedView = Utilities.sharedInstance.createView(cornerRadius: 5.0, backgorundColor: .orange)
        promotedImageView = Utilities.sharedInstance.createImageViewWith(imageName: "promoted", backgroundColor: .clear, contentMode: .scaleAspectFit, corner: 0.0)
        promotedView.addSubview(promotedImageView)
        promotedView.isHidden = true
        
        contentView.addSubview(venueImageView)
        contentView.addSubview(venueNameLabel)
        contentView.addSubview(venueCategoryLabel)
        contentView.addSubview(venueAddressLabel)
        contentView.addSubview(promotedView)
    }
    
    func setupConstraints() {
        venueImageView.snp.makeConstraints { (make) in
            make.left.equalTo(contentView).offset(20)
            make.centerY.equalTo(contentView.snp.centerY)
            make.height.equalTo(contentView.snp.height).multipliedBy(0.7)
            make.width.equalTo(contentView.snp.width).dividedBy(2.9)
        }
        
        venueNameLabel.snp.makeConstraints { (make) in
            make.left.equalTo(venueImageView.snp.right).offset(10)
            make.right.equalTo(contentView).offset(-10)
            make.top.equalTo(venueImageView).offset(3)
        }
        
        venueCategoryLabel.snp.makeConstraints { (make) in
            make.left.right.equalTo(venueNameLabel)
            make.centerY.equalTo(venueImageView.snp.centerY)
        }
        
        venueAddressLabel.snp.makeConstraints { (make) in
            make.left.right.equalTo(venueNameLabel)
            make.top.equalTo(venueCategoryLabel.snp.bottom).offset(5)
        }
        
        promotedView.snp.makeConstraints { (make) in
            make.left.equalTo(venueImageView).offset(-8)
            make.top.equalTo(venueImageView).offset(5)
            make.height.width.equalTo(venueImageView.snp.height).dividedBy(3.1)
        }
        
        promotedImageView.snp.makeConstraints { (make) in
            make.centerY.equalTo(promotedView.snp.centerY)
            make.centerX.equalTo(promotedView.snp.centerX)
            make.width.height.equalTo(promotedView.snp.height).multipliedBy(0.6)
        }
    }

    //MARK: - SETUP CELL
    func setupCellWith(venue: Venue) {
        if let venueImage = venue.picture_url {
            if let imageUrl = URL(string: venueImage) {
                venueImageView.kf.setImage(with: imageUrl)
            }
        }
        if let venueName = venue.name {
            venueNameLabel.text = venueName
        }
        
        if let venueCat = venue.subtitle {
            venueCategoryLabel.text = venueCat
        }
        
        if let venueAddress = venue.address {
            venueAddressLabel.text = venueAddress
        }
        
        if let promoted = venue.bat {
            if promoted == 0 {
                promotedView.isHidden = true
            } else {
                promotedView.isHidden = false
            }
        }
    }
}
