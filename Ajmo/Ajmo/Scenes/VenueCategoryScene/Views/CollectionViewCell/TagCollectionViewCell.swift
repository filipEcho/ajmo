//
//  TagCollectionViewCell.swift
//  Ajmo
//
//  Created by Filip Stevanoski on 10/7/20.
//  Copyright © 2020 Filip Stevanoski. All rights reserved.
//

import UIKit

class TagCollectionViewCell: UICollectionViewCell {
    
    //Properties
    var holderView: UIView!
    var tagLabel: UILabel!
    
    //MARK: - INITIALIZATION
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - SETUP VIEWS AND CONSTRAINTS
    func setupViews() {
        contentView.backgroundColor = .clear
        
        holderView = Utilities.sharedInstance.createView(cornerRadius: 10.0, backgorundColor: .white)
    
        tagLabel = Utilities.sharedInstance.createLabelWith(text: "", txtAlignment: .center, font: .systemFont(ofSize: 15, weight: .semibold), textColor: .black, backgroundColor: .clear)
        
        holderView.addSubview(self.tagLabel)
        contentView.addSubview(self.holderView)
    }
    
    func setupConstraints() {
        holderView.snp.makeConstraints { (make) in
            make.left.top.equalTo(self.contentView)
            make.height.equalTo(self.contentView.snp.height).multipliedBy(0.98)
            make.width.equalTo(self.contentView.snp.width)
        }
        
        tagLabel.snp.makeConstraints { (make) in
            make.left.equalTo(holderView).offset(5)
            make.right.equalTo(holderView).offset(-5)
            make.centerY.equalTo(holderView.snp.centerY)
        }
    }
    
    //MARK: - SETUP CELL
    //Setup Cell for CategoryScene with Popular Tags
    func setupCellWith(popularTag: PopularTag) {
        tagLabel.font = .systemFont(ofSize: 15, weight: .semibold)
        if let tagName = popularTag.tag?.name {
            tagLabel.text = tagName
        }
        
        if let tagsCount = popularTag.tags {
            tagLabel.text?.append(" (" + "\(tagsCount)" + ")")
        }
        
        if let tagColorHex = popularTag.tag?.color {
            let tagColor = Utilities.sharedInstance.colorFromHexCode(hex: tagColorHex)
            if tagColor.isLight {
                tagLabel.textColor = .black
            } else {
                tagLabel.textColor = .white
            }
            
            holderView.backgroundColor = tagColor
        }
    }
    
    //Setup Cell for VenueScene with Primary tag
    func setupCellWithTag(primaryTag: Tag, isForPrimaryTag: Bool) {
        tagLabel.font = .systemFont(ofSize: 13, weight: .regular)
        if let tagName = primaryTag.name {
            tagLabel.text = tagName
        }
        
        if isForPrimaryTag {
            if let tagColorHex = primaryTag.color {
                let tagColor = Utilities.sharedInstance.colorFromHexCode(hex: tagColorHex)
                if tagColor.isLight {
                    tagLabel.textColor = .black
                } else {
                    tagLabel.textColor = .white
                }
                holderView.backgroundColor = tagColor
                holderView.layer.borderColor = UIColor.clear.cgColor
                holderView.layer.borderWidth = 0.0
            }
        } else {
            tagLabel.textColor = .black
            holderView.backgroundColor = .clear
            holderView.layer.borderColor = UIColor.lightGray.cgColor
            holderView.layer.borderWidth = 0.7
        }
    }
}
