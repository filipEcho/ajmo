//
//  PromotedVenueCollectionViewCell.swift
//  Ajmo
//
//  Created by Filip Stevanoski on 10/7/20.
//  Copyright © 2020 Filip Stevanoski. All rights reserved.
//

import UIKit

class PromotedVenueCollectionViewCell: UICollectionViewCell {
    
    //Properties
    var holderView: UIView!
    var promotedView: UIView!
    var promotedImageView: UIImageView!
    var venueImageView: UIImageView!
    var nameHelperView: UIView!
    var venueNameLabel: UILabel!
    var venueCategoryLabel: UILabel!
    var hotPickLabel: UILabel!
    
    //MARK: - INITIALIZATION
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - SETUP VIEWS AND CONSTRAINTS
    func setupViews() {
        contentView.backgroundColor = Theme.sharedInstance.getBackgroundColor()
        
        holderView = Utilities.sharedInstance.createView(cornerRadius: 10.0, backgorundColor: .white)
        holderView.frame = CGRect(x: 0, y: 0, width: contentView.frame.size.width, height: contentView.frame.size.height)
        Utilities.sharedInstance.addShadowTo(view: holderView)
        holderView.layer.masksToBounds = true
        
        promotedView = Utilities.sharedInstance.createView(cornerRadius: 5.0, backgorundColor: .orange)
        
        hotPickLabel = Utilities.sharedInstance.createLabelWith(text: Constants.VenueCategory.kHotPicks, txtAlignment: .left, font: .systemFont(ofSize: 12, weight: .semibold), textColor: .white, backgroundColor: .clear)
        promotedView.addSubview(hotPickLabel)
        
        promotedImageView = Utilities.sharedInstance.createImageViewWith(imageName: Constants.VenueCategory.kPromotedImage, backgroundColor: .clear, contentMode: .scaleAspectFit, corner: 0.0)
        promotedView.addSubview(promotedImageView)
        
        venueImageView = Utilities.sharedInstance.createImageViewWith(imageName: "", backgroundColor: .clear, contentMode: .scaleAspectFill, corner: 0.0)
        holderView.addSubview(self.venueImageView)
        
        nameHelperView = Utilities.sharedInstance.createView(cornerRadius: 0.0, backgorundColor: .clear)
        
        venueNameLabel = Utilities.sharedInstance.createLabelWith(text: "", txtAlignment: .left, font: .systemFont(ofSize: 16, weight: .semibold), textColor: .black, backgroundColor: .clear)
        nameHelperView.addSubview(self.venueNameLabel)
        
        venueCategoryLabel = Utilities.sharedInstance.createLabelWith(text: "", txtAlignment: .left, font: .systemFont(ofSize: 14, weight: .regular), textColor: .secondaryLabel, backgroundColor: .clear)
        nameHelperView.addSubview(self.venueCategoryLabel)
        
        holderView.addSubview(self.nameHelperView)
        contentView.addSubview(self.holderView)
        contentView.addSubview(self.promotedView)
    }
    
    func setupConstraints() {
        holderView.snp.makeConstraints { (make) in
            make.top.equalTo(contentView)
            make.left.equalTo(contentView)
            make.right.equalTo(contentView)
            make.bottom.equalTo(contentView)
        }
        
        venueImageView.snp.makeConstraints { (make) in
            make.left.right.top.equalTo(holderView)
            make.height.equalTo(holderView.snp.height).multipliedBy(0.7)
        }
        
        promotedView.snp.makeConstraints { (make) in
            make.top.equalTo(venueImageView).offset(10)
            make.left.equalTo(venueImageView).offset(-10)
            make.height.equalTo(venueImageView.snp.height).dividedBy(6.5)
            make.width.equalTo(venueImageView.snp.width).dividedBy(3.2)
        }
        
        promotedImageView.snp.makeConstraints { (make) in
            make.centerY.equalTo(promotedView.snp.centerY)
            make.left.equalTo(promotedView).offset(5)
            make.width.height.equalTo(promotedView.snp.height).multipliedBy(0.6)
        }
        
        hotPickLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(promotedView.snp.centerY)
            make.right.equalTo(self.promotedView).offset(-7)
        }
        
        nameHelperView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalTo(self.holderView)
            make.top.equalTo(venueImageView.snp.bottom)
        }
        
        venueNameLabel.snp.makeConstraints { (make) in
            make.bottom.equalTo(nameHelperView.snp.centerY)
            make.left.equalTo(holderView).offset(20)
        }
        
        venueCategoryLabel.snp.makeConstraints { (make) in
            make.top.equalTo(venueNameLabel.snp.bottom).offset(10)
            make.left.equalTo(venueNameLabel)
        }
        
        
    }
    
    //MARK: - SETUP CELL
    func setupCellWith(promotedVenue: Venue) {
        if let venueImage = promotedVenue.picture_url {
            if let imageUrl = URL(string: venueImage) {
                venueImageView.kf.setImage(with: imageUrl)
            }
        }
        if let venueName = promotedVenue.name {
            venueNameLabel.text = venueName
        }
        
        if let venueDesc = promotedVenue.subtitle {
            venueCategoryLabel.text = venueDesc
        }
    }
}
