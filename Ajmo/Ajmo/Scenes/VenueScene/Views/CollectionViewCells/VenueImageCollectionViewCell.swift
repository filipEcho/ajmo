//
//  VenueImageCollectionViewCell.swift
//  Ajmo
//
//  Created by Filip Stevanoski on 10/8/20.
//  Copyright © 2020 Filip Stevanoski. All rights reserved.
//

import UIKit

class VenueImageCollectionViewCell: UICollectionViewCell {
    
    //Properties
    var venueImageView: UIImageView!
    
    //MARK: - INITIALIZATION
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        contentView.backgroundColor = .clear
        
        venueImageView = Utilities.sharedInstance.createImageViewWith(imageName: "", backgroundColor: .clear, contentMode: .scaleAspectFit, corner: 0.0)
        
        contentView.addSubview(venueImageView)
    }
    
    func setupConstraints() {
        venueImageView.snp.makeConstraints { (make) in
            make.edges.equalTo(contentView)
        }
    }
    
    func setupCellWit(image: Gallery) {
        if let imageUrl = Utilities.sharedInstance.getUrlFromImageString(imageString: image.picture) {
            venueImageView.kf.setImage(with: imageUrl)
        }
    }
}
