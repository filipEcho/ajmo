//
//  OpenHoursView.swift
//  Ajmo
//
//  Created by Filip Stevanoski on 10/8/20.
//  Copyright © 2020 Filip Stevanoski. All rights reserved.
//

import UIKit

protocol OpenHoursViewDelegate {
    func okButtonAction()
}

class OpenHoursView: UIView {
    
    //Parameters
    var holderView: UIView!
    var hoursLabel: UILabel!
    var tableView: UITableView!
    var openIndicatorLabel: UILabel!
    var daysTableView: UITableView!
    var okButton: UIButton!
    var delegate: OpenHoursViewDelegate?
    
    var workingHours = [WorkingHours]()
    
    //MARK: - INITIALIZATION
    init(frame : CGRect, workingHours: [WorkingHours]) {
        super.init(frame: frame)
        self.workingHours = workingHours
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - SETUP VIEWS AND CONSTRAINTS
    func setupViews() {
        self.backgroundColor = .clear
        
        holderView = Utilities.sharedInstance.createView(cornerRadius: 10.0, backgorundColor: .white)
        holderView.frame = CGRect(x: 0.0, y: 0.0, width: self.frame.size.width*0.7, height: self.frame.size.height/2.7)
        Utilities.sharedInstance.addShadowTo(view: holderView)
        
        hoursLabel = Utilities.sharedInstance.createLabelWith(text: "Hours", txtAlignment: .center, font: .systemFont(ofSize: 16, weight: .semibold), textColor: .black, backgroundColor: .clear)
        holderView.addSubview(hoursLabel)
        
        openIndicatorLabel = Utilities.sharedInstance.createLabelWith(text: "", txtAlignment: .center, font: .systemFont(ofSize: 13, weight: .semibold), textColor: .systemGreen, backgroundColor: .clear)
        self.checkIfVenueIsOpen()
        
        holderView.addSubview(openIndicatorLabel)
        
        //TableView
        tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.bounces = false
        tableView.tableFooterView = UIView()
        tableView.register(HoursTableViewCell.self, forCellReuseIdentifier: Constants.Venue.Cells.kHoursCell)
        holderView.addSubview(tableView)
        
        okButton = Utilities.sharedInstance.createButtonWith(title: Constants.Venue.Strings.kOK, titleColor: .systemYellow, font: .systemFont(ofSize: 18, weight: .semibold), txtAligment: .center, imageName: nil, corner: 0.0, backgroundColor: .clear, tag: 1)
        okButton.addTarget(self, action: #selector(okButtonTouched), for: .touchUpInside)
        holderView.addSubview(okButton)
    
        self.addSubview(holderView)
    }
    
    func setupConstraints() {
        holderView.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.snp.centerY).offset(-25)
            make.width.equalTo(self.snp.width).multipliedBy(0.7)
            if Utilities.sharedInstance.iphoneType(type: .IPhoneXorBigger) {
                make.height.equalTo(self.snp.height).dividedBy(2.35)
            } else if Utilities.sharedInstance.iphoneType(type: .IPhonePlus) {
                make.height.equalTo(self.snp.height).dividedBy(2.1)
            } else {
                make.height.equalTo(self.snp.height).dividedBy(1.9)
            }
            
            make.centerX.equalTo(self.snp.centerX)
        }
        
        okButton.snp.makeConstraints { (make) in
            make.centerX.equalTo(holderView.snp.centerX)
            make.bottom.equalTo(holderView).offset(-5)
            make.width.equalTo(holderView.snp.width).dividedBy(4)
            make.height.equalTo(holderView.snp.height).dividedBy(9.5)
        }
        
        tableView.snp.makeConstraints { (make) in
            make.centerX.equalTo(holderView.snp.centerX)
            make.height.equalTo(7*25)
            make.width.equalTo(holderView)
            make.bottom.equalTo(okButton.snp.top).offset(-15)
        }
        
        hoursLabel.snp.makeConstraints { (make) in
            make.top.equalTo(holderView).offset(25)
            make.centerX.equalTo(holderView.snp.centerX)
        }
        
        openIndicatorLabel.snp.makeConstraints { (make) in
            make.top.equalTo(hoursLabel.snp.bottom).offset(15)
            make.centerX.equalTo(holderView.snp.centerX)
        }
    }
    
    //MARK: - BUTTON ACTION
    @objc func okButtonTouched() {
        self.delegate?.okButtonAction()
    }
    
    func checkIfVenueIsOpen() {
        let currentDay = Utilities.sharedInstance.getTheCurrentWeekDay() - 1
        if workingHours.count > 0 {
            for day in workingHours {
                if day.day == currentDay {
                    let startHour = workingHours[currentDay].start ?? ""
                    let endHour = workingHours[currentDay].end ?? ""

                    if Utilities.sharedInstance.isVenueOpen(startString: startHour, endString: endHour) {
                        openIndicatorLabel.text = Constants.Venue.Strings.kOpen
                        openIndicatorLabel.textColor = .systemGreen
                    } else {
                        openIndicatorLabel.text = Constants.Venue.Strings.kClosed
                        openIndicatorLabel.textColor = .systemRed
                    }
                    break
                }
            }
        }
    }
}

//MARK: - TABLEVIEW DELEGATE AND DATASOURCE
extension OpenHoursView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return workingHours.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 25.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Venue.Cells.kHoursCell, for: indexPath) as! HoursTableViewCell
        cell.setupCellWith(workingHours: workingHours[indexPath.row])
            
        return cell
    }
}
