//
//  SecondaryTagsTableViewCell.swift
//  Ajmo
//
//  Created by Filip Stevanoski on 10/8/20.
//  Copyright © 2020 Filip Stevanoski. All rights reserved.
//

import UIKit
import TagListView

protocol SecondaryTagsTableViewCellDelegate {
    func tagPressed(tag : Tag)
}

class SecondaryTagsTableViewCell: UITableViewCell {
    
    //Properties
    var tagListView : TagListView!
    var tagsCollectionView : UICollectionView!
    var secondaryTagLabel: UILabel!
    var tags = [Tag]()
    var tagStrings = [String]()
    
    //Delegate
    var delegate : SecondaryTagsTableViewCellDelegate?
    
    //MARK:- INITIALIZATION
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:)has not been implemented")
    }
    
    //MARK:- SETUP VIEWS AND CONSTRAINTS
    func setupViews() {
        contentView.backgroundColor = Theme.sharedInstance.getBackgroundColor()
        selectionStyle = .none
        
        secondaryTagLabel = Utilities.sharedInstance.createLabelWith(text: "", txtAlignment: .left, font: .systemFont(ofSize: 16, weight: .bold), textColor: .black, backgroundColor: .clear)
        
        
        tagListView = TagListView()
        self.tagListView.alignment = .left
        self.tagListView.tagBackgroundColor = .white
        self.tagListView.textColor = .black
        self.tagListView.borderColor = .black
        self.tagListView.borderWidth = 1.0
        self.tagListView.paddingX = 15
        self.tagListView.paddingY = 15
        self.tagListView.tagBackgroundColor = .clear
        self.tagListView.textFont = UIFont.systemFont(ofSize: 16)
        self.tagListView.cornerRadius = 10
        self.tagListView.delegate = self
        
        contentView.addSubview(secondaryTagLabel)
        contentView.addSubview(tagListView)
        //contentView.addSubview(tagsCollectionView)
    }
    
    func setupConstraints() {
        secondaryTagLabel.snp.makeConstraints { (make) in
            make.left.equalTo(contentView).offset(20)
            make.top.equalTo(contentView).offset(10)
            make.right.equalTo(contentView).offset(-10)
        }
        
        tagListView.snp.makeConstraints { (make) in
            make.top.equalTo(secondaryTagLabel.snp.bottom).offset(20)
            make.left.equalTo(contentView).offset(20)
            make.right.equalTo(contentView).offset(-20)
            make.bottom.equalTo(contentView).offset(-10.0)
        }
    }

    //MARK: - SETUP CELL
    func setupCellWith(venue: Venue) {
        if let secondaryTags = venue.allTags?.secondaryTags {
            self.tags = secondaryTags
            secondaryTagLabel.text = "What to expect"
            if tagListView.tagViews.isEmpty {
                var tagsStrings = [String]()
                for tag in tags {
                    if let name = tag.name {
                        tagsStrings.append(name)
                    }
                }
                self.tagListView.addTags(tagsStrings)
                self.contentView.layoutIfNeeded()
            }
        }
    }
}

extension SecondaryTagsTableViewCell: TagListViewDelegate {
    func tagPressed(_ itle: String, tagView: TagView, sender: TagListView) {
        if let index = tagListView.tagViews.firstIndex(of: tagView) {
            self.delegate?.tagPressed(tag: self.tags[index])
        }
    }
}
