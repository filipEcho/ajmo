//
//  VenueInfoTableViewCell.swift
//  Ajmo
//
//  Created by Filip Stevanoski on 10/8/20.
//  Copyright © 2020 Filip Stevanoski. All rights reserved.
//

import UIKit

class VenueInfoTableViewCell: UITableViewCell {
    
    //Properties
    var infoIconImageView: UIImageView!
    var infoLabel: UILabel!
    var openIndicatorLabel: UILabel!

    //MARK:- INITIALIZATION
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- SETUP VIEWS AND CONSTRAINTS
    func setupViews() {
        contentView.backgroundColor = Theme.sharedInstance.getBackgroundColor()
        selectionStyle = .none
        
        infoIconImageView = Utilities.sharedInstance.createImageViewWith(imageName: "", backgroundColor: .clear, contentMode: .scaleAspectFit, corner: 0.0)
        infoLabel = Utilities.sharedInstance.createLabelWith(text: "", txtAlignment: .left, font: .systemFont(ofSize: 15, weight: .regular), textColor: .black, backgroundColor: .clear)
        openIndicatorLabel = Utilities.sharedInstance.createLabelWith(text: "", txtAlignment: .left, font: .systemFont(ofSize: 13, weight: .semibold), textColor: .systemGreen, backgroundColor: .clear)
        
        contentView.addSubview(infoIconImageView)
        contentView.addSubview(infoLabel)
        contentView.addSubview(openIndicatorLabel)
    }
    
    func setupConstraints() {
        infoIconImageView.snp.makeConstraints { (make) in
            make.left.equalTo(contentView).offset(20)
            make.centerY.equalTo(contentView.snp.centerY)
            make.width.height.equalTo(contentView.snp.height).multipliedBy(0.4)
        }
        
        infoLabel.snp.makeConstraints { (make) in
            make.left.equalTo(infoIconImageView.snp.right).offset(20)
            //make.right.equalTo(contentView).offset(20)
            make.centerY.equalTo(contentView.snp.centerY)
        }
        
        openIndicatorLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(contentView.snp.centerY)
            make.left.equalTo(infoLabel.snp.right).offset(10)
        }
    }
    
    //MARK: - SETUP CELL
    func setupCellWith(venue: Venue, index: Int) {
        if index == 0 {
            setupCellForWorkingHours(venue: venue)
        } else if index == 1 {
            openIndicatorLabel.isHidden = true
            infoLabel.text = venue.address ?? ""
            self.infoIconImageView.image = UIImage(named: Constants.Venue.Images.kLocationImageName)
        } else {
            openIndicatorLabel.isHidden = true
            infoLabel.text = venue.telephone ?? ""
            self.infoIconImageView.image = UIImage(named: Constants.Venue.Images.kPhoneImageName)
        }
    }
    
    func setupCellForWorkingHours(venue: Venue) {
        let currentDay = Utilities.sharedInstance.getTheCurrentWeekDay() - 1
        if let workingHours = venue.working_hours {
            if workingHours.count > 0 {
                for day in workingHours {
                    if day.day == currentDay {
                        let startHour = venue.working_hours?[currentDay].start ?? ""
                        let endHour = venue.working_hours?[currentDay].end ?? ""
                        infoLabel.text = startHour + " - " + endHour
                        openIndicatorLabel.isHidden = false
                        if Utilities.sharedInstance.isVenueOpen(startString: startHour, endString: endHour) {
                            openIndicatorLabel.text = Constants.Venue.Strings.kOpen
                            openIndicatorLabel.textColor = .systemGreen
                        } else {
                            openIndicatorLabel.text = Constants.Venue.Strings.kClosed
                            openIndicatorLabel.textColor = .systemRed
                        }
                        self.infoIconImageView.image = UIImage(named: Constants.Venue.Images.kClockImageName)
                        break
                    }
                }
            }
        }
    }
}
