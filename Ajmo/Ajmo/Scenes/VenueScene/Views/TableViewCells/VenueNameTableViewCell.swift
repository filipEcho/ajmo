//
//  VenueNameTableViewCell.swift
//  Ajmo
//
//  Created by Filip Stevanoski on 10/8/20.
//  Copyright © 2020 Filip Stevanoski. All rights reserved.
//

import UIKit

class VenueNameTableViewCell: UITableViewCell {
    
    //Properties
    var venueNameLabel: UILabel!
    var venueCategoryLabel: UILabel!

    //MARK:- INITIALIZATION
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- SETUP VIEWS AND CONSTRAINTS
    func setupViews() {
        contentView.backgroundColor = Theme.sharedInstance.getBackgroundColor()
        selectionStyle = .none
        
        venueNameLabel = Utilities.sharedInstance.createLabelWith(text: "", txtAlignment: .left, font: .systemFont(ofSize: 24, weight: .bold), textColor: .black, backgroundColor: .clear)
        venueCategoryLabel = Utilities.sharedInstance.createLabelWith(text: "", txtAlignment: .left, font: .systemFont(ofSize: 13, weight: .regular), textColor: .secondaryLabel, backgroundColor: .clear)
        
        contentView.addSubview(venueNameLabel)
        contentView.addSubview(venueCategoryLabel)
    }
    
    func setupConstraints() {
        venueNameLabel.snp.makeConstraints { (make) in
            make.left.equalTo(contentView).offset(20)
            make.right.equalTo(contentView).offset(20)
            make.top.equalTo(contentView).offset(40)
        }
        
        venueCategoryLabel.snp.makeConstraints { (make) in
            make.left.right.equalTo(venueNameLabel)
            make.top.equalTo(venueNameLabel.snp.bottom).offset(10)
            make.bottom.equalTo(contentView).offset(-15)
        }
    }
    
    //MARK: - SETUP CELL
    func setupCellWith(venue: Venue) {
        if let venueName = venue.name {
            venueNameLabel.text = venueName
        }
        
        if let venueCat = venue.subtitle {
            venueCategoryLabel.text = venueCat
        }
    }
}
