//
//  SmokingTableViewCell.swift
//  Ajmo
//
//  Created by Filip Stevanoski on 10/8/20.
//  Copyright © 2020 Filip Stevanoski. All rights reserved.
//

import UIKit

class SmokingTableViewCell: UITableViewCell {
    
    //Properties
    var smokingIconImageView: UIImageView!
    var smokingLabel: UILabel!

    //MARK:- INITIALIZATION
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- SETUP VIEWS AND CONSTRAINTS
    func setupViews() {
        contentView.backgroundColor = Theme.sharedInstance.getBackgroundColor()
        selectionStyle = .none
        
        smokingIconImageView = Utilities.sharedInstance.createImageViewWith(imageName: "", backgroundColor: .clear, contentMode: .scaleAspectFit, corner: 0.0)
        smokingLabel = Utilities.sharedInstance.createLabelWith(text: "", txtAlignment: .left, font: .systemFont(ofSize: 16, weight: .regular), textColor: .black, backgroundColor: .clear)
        
        contentView.addSubview(smokingIconImageView)
        contentView.addSubview(smokingLabel)
    }
    
    func setupConstraints() {
        smokingIconImageView.snp.makeConstraints { (make) in
            make.left.equalTo(contentView).offset(24)
            make.centerY.equalTo(contentView.snp.centerY)
            make.width.height.equalTo(contentView.snp.height).multipliedBy(0.35)

        }
        smokingLabel.snp.makeConstraints { (make) in
            make.left.equalTo(smokingIconImageView.snp.right).offset(20)
            make.centerY.equalTo(contentView.snp.centerY)
        }
    }
    
    //MARK: - SETUP CELL
    func setupCellWith(venue: Venue) {
        if let smoking = venue.smoking_area {
            if smoking != 0 {
                smokingLabel.text = Constants.Venue.Strings.kSmoking
                smokingIconImageView.image = UIImage(named: Constants.Venue.Images.kSmokingImageName)
            } else {
                smokingLabel.text = Constants.Venue.Strings.kNoSmoking
                smokingIconImageView.image = UIImage(named: Constants.Venue.Images.kNoSmokingImageName)
            }
        }
    }
}
