//
//  VenueTagsTableViewCell.swift
//  Ajmo
//
//  Created by Filip Stevanoski on 10/8/20.
//  Copyright © 2020 Filip Stevanoski. All rights reserved.
//

import UIKit

protocol VenueTagsTableViewCellDelegate {
    func selectedTag(tag: Tag)
}

class VenueTagsTableViewCell: UITableViewCell {
    
    //Properties
    var tagsCollectionView: UICollectionView!
    var primaryTagsLabel: UILabel!
    var tags = [Tag]()
    
    var delegate: VenueTagsTableViewCellDelegate?

    //MARK:- INITIALIZATION
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- SETUP VIEWS AND CONSTRAINTS
    func setupViews() {
        contentView.backgroundColor = Theme.sharedInstance.getBackgroundColor()
        selectionStyle = .none
        
        primaryTagsLabel = Utilities.sharedInstance.createLabelWith(text: "", txtAlignment: .left, font: .systemFont(ofSize: 16, weight: .bold), textColor: .black, backgroundColor: .clear)
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 10
        layout.minimumInteritemSpacing = 0
        //layout.sectionInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)
        
        tagsCollectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        tagsCollectionView.backgroundColor = .clear
        tagsCollectionView.register(TagCollectionViewCell.self, forCellWithReuseIdentifier: Constants.Venue.Cells.kTagsCell)
        tagsCollectionView.showsHorizontalScrollIndicator = false
        tagsCollectionView.delegate = self
        tagsCollectionView.dataSource = self
        
        contentView.addSubview(primaryTagsLabel)
        contentView.addSubview(tagsCollectionView)
    }
    
    func setupConstraints() {
        primaryTagsLabel.snp.makeConstraints { (make) in
            make.left.equalTo(contentView).offset(20)
            make.top.equalTo(contentView).offset(10)
        }
        
        tagsCollectionView.snp.makeConstraints { (make) in
            make.top.equalTo(primaryTagsLabel.snp.bottom).offset(20)
            make.left.equalTo(contentView).offset(20)
            make.right.equalTo(contentView).offset(-20)
            make.height.equalTo(contentView.snp.height).multipliedBy(0.4)
        }
    }

    //MARK: - SETUP CELL
    func setupCellWith(venue: Venue) {
        if let primaryTags = venue.allTags?.primaryTags {
            self.tags = primaryTags
            if let primaryTag = venue.primary_tag_group {
                primaryTagsLabel.text = primaryTag
            }
        }
    }
}

//MARK: - UICOLLECTIONVIEW DELEGATE, DATASOURCE & FLOW LAYOUT
extension VenueTagsTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tags.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.Venue.Cells.kTagsCell, for: indexPath) as! TagCollectionViewCell
        cell.setupCellWithTag(primaryTag: tags[indexPath.row], isForPrimaryTag: true)
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //Set item size depending of the number of primary tags
        if tags.count == 1 {
            return CGSize(width: contentView.frame.size.width - 40 , height: tagsCollectionView.frame.size.height)
        } else if tags.count == 2 {
            return CGSize(width: (contentView.frame.size.width - 50)/2 , height: tagsCollectionView.frame.size.height)
        } else {
            return CGSize(width: (contentView.frame.size.width - 60)/3 , height: tagsCollectionView.frame.size.height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.selectedTag(tag: tags[indexPath.row])
    }
}
