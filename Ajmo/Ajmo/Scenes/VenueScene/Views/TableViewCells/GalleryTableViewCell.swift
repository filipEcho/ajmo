//
//  GalleryTableViewCell.swift
//  Ajmo
//
//  Created by Filip Stevanoski on 10/8/20.
//  Copyright © 2020 Filip Stevanoski. All rights reserved.
//

import UIKit

protocol GalleryTableViewCellDelegate {
    func imageSelectedAtIndex(index: Int)
}

class GalleryTableViewCell: UITableViewCell {
    
    //Properties
    var venueBigImageView: UIImageView!
    var firstSmallImageView: UIImageView!
    var secondSmallImageView: UIImageView!
    var numberOfimagesView: UIView!
    var numberOfImagesLabel: UILabel!
    
    var delegate: GalleryTableViewCellDelegate?
    
    //MARK:- INITIALIZATION
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- SETUP VIEWS AND CONSTRAINTS
    func setupViews() {
        contentView.backgroundColor = Theme.sharedInstance.getBackgroundColor()
        
        venueBigImageView = Utilities.sharedInstance.createImageViewWith(imageName: "", backgroundColor: .clear, contentMode: .scaleAspectFill, corner: 0.0)
        venueBigImageView.isUserInteractionEnabled = true
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(bigImageTapped(tapGestureRecognizer:)))
        venueBigImageView.addGestureRecognizer(tapGestureRecognizer)
        
        firstSmallImageView = Utilities.sharedInstance.createImageViewWith(imageName: "", backgroundColor: .clear, contentMode: .scaleAspectFill, corner: 0.0)
        firstSmallImageView.isUserInteractionEnabled = true
        let firstSmallImagetapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(firstSmallImageTapped(tapGestureRecognizer:)))
        firstSmallImageView.addGestureRecognizer(firstSmallImagetapGestureRecognizer)
        
        secondSmallImageView = Utilities.sharedInstance.createImageViewWith(imageName: "", backgroundColor: .clear, contentMode: .scaleAspectFill, corner: 0.0)
        secondSmallImageView.isUserInteractionEnabled = true
        let secondSmallImagetapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(firstSmallImageTapped(tapGestureRecognizer:)))
        secondSmallImageView.addGestureRecognizer(secondSmallImagetapGestureRecognizer)
        
        numberOfimagesView = Utilities.sharedInstance.createView(cornerRadius: 1.0, backgorundColor: UIColor(white: 0.0, alpha: 0.1))
        numberOfimagesView.layer.borderColor = UIColor.white.cgColor
        numberOfimagesView.layer.borderWidth = 1.2
        
        numberOfImagesLabel = Utilities.sharedInstance.createLabelWith(text: "", txtAlignment: .center, font: .systemFont(ofSize: 14, weight: .semibold), textColor: .white, backgroundColor: .clear)
        numberOfimagesView.addSubview(numberOfImagesLabel)
        
        contentView.addSubview(venueBigImageView)
        contentView.addSubview(firstSmallImageView)
        contentView.addSubview(secondSmallImageView)
        contentView.addSubview(numberOfimagesView)
    }
    
    func setupConstraints() {
        venueBigImageView.snp.makeConstraints { (make) in
            make.left.equalTo(contentView)
            make.centerY.equalTo(contentView.snp.centerY)
            make.height.equalTo(contentView.snp.height).multipliedBy(0.9)
            make.width.equalTo(contentView.snp.width).multipliedBy(0.65)
        }
        
        firstSmallImageView.snp.makeConstraints { (make) in
            make.right.equalTo(contentView)
            make.top.equalTo(venueBigImageView)
            make.left.equalTo(venueBigImageView.snp.right).offset(10)
            make.bottom.equalTo(venueBigImageView.snp.centerY).offset(-5)
        }
        
        secondSmallImageView.snp.makeConstraints { (make) in
            make.right.width.height.equalTo(firstSmallImageView)
            make.top.equalTo(venueBigImageView.snp.centerY).offset(5)
        }
        
        numberOfimagesView.snp.makeConstraints { (make) in
            make.left.equalTo(venueBigImageView).offset(25)
            make.height.equalTo(venueBigImageView.snp.height).dividedBy(6)
            make.width.equalTo(numberOfimagesView.snp.height).multipliedBy(1.05)
            make.bottom.equalTo(venueBigImageView).offset(-15)
        }
        
        numberOfImagesLabel.snp.makeConstraints { (make) in
            make.left.right.equalTo(numberOfimagesView)
            make.centerY.equalTo(numberOfimagesView.snp.centerY)
        }
    }
    
    @objc func bigImageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        self.delegate?.imageSelectedAtIndex(index: 0)
    }
    
    @objc func firstSmallImageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        self.delegate?.imageSelectedAtIndex(index: 1)
    }
    
    @objc func secondSmallImageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        self.delegate?.imageSelectedAtIndex(index: 2)
    }
    
    //MARK:- SETUP CELL WITH IMAGES
    func setupCellWith(images: [Gallery]) {
        if images.count > 3 { // If there are more than 3 images, skip the first one
            self.setImage(imageView: venueBigImageView, imageName: images[1].picture)
            self.setImage(imageView: firstSmallImageView, imageName: images[2].picture)
            self.setImage(imageView: secondSmallImageView, imageName: images[3].picture)
        } else { // Logic for 3 or less images
            self.setImage(imageView: venueBigImageView, imageName: images[0].picture)
            if images.count > 2 {
                self.setImage(imageView: firstSmallImageView, imageName: images[1].picture)
                self.setImage(imageView: secondSmallImageView, imageName: images[2].picture)
            } else {
                if images.count == 2 {
                    self.setImage(imageView: firstSmallImageView, imageName: images[1].picture)
                }
            }
        }
        
        numberOfImagesLabel.text = "\(images.count)"
        self.makeChangesDependingOftheNumberOfImages(numberOfImages: images.count)
    }
    
    func setImage(imageView: UIImageView, imageName: String?) {
        if let url = Utilities.sharedInstance.getUrlFromImageString(imageString: imageName) {
            imageView.kf.setImage(with: url)
        }
    }
    
    //MARK:- HELPERS
    func makeChangesDependingOftheNumberOfImages(numberOfImages: Int) {
        if numberOfImages == 1 { // Logic for one image
            secondSmallImageView.isHidden = true
            firstSmallImageView.isHidden = true
        } else { //Logic for more than one image
            firstSmallImageView.isHidden = false
            if numberOfImages < 3 {
                secondSmallImageView.isHidden = true
            } else {
                secondSmallImageView.isHidden = false
            }
        }
        if numberOfImages == 1 {
            self.remakeContraintsForBigImage(onlyOneImage: numberOfImages == 1)
        }
        
        if numberOfImages == 2 {
            remakeConstraintsForSecondImage(onlyTwoImages: true)
        }
    }
    
    // Remake Constraints for the Big ImageView
    func remakeContraintsForBigImage(onlyOneImage: Bool) {
        venueBigImageView.snp.remakeConstraints { (remake) in
            remake.left.equalTo(contentView)
            remake.centerY.equalTo(contentView.snp.centerY)
            remake.height.equalTo(contentView.snp.height).multipliedBy(0.9)
            if onlyOneImage {
                remake.width.equalTo(contentView.snp.width)
            } else {
                remake.width.equalTo(contentView.snp.width).multipliedBy(0.65)
            }
        }
    }
    
    //Remake Constraints for the First Small ImageView
    func remakeConstraintsForSecondImage(onlyTwoImages: Bool) {
        firstSmallImageView.snp.makeConstraints { (make) in
            make.right.equalTo(contentView)
            make.top.equalTo(venueBigImageView)
            make.left.equalTo(venueBigImageView.snp.right).offset(10)
            if onlyTwoImages {
                make.bottom.equalTo(venueBigImageView)
            } else {
                make.bottom.equalTo(venueBigImageView.snp.centerY).offset(-5)
            }
        }
    }
}
