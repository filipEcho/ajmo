//
//  VenueDescriptionTableViewCell.swift
//  Ajmo
//
//  Created by Filip Stevanoski on 10/8/20.
//  Copyright © 2020 Filip Stevanoski. All rights reserved.
//

import UIKit

class VenueDescriptionTableViewCell: UITableViewCell {
    
    //Properties
    var idealPlaceLabel: UILabel!
    var venueDescLabel: UILabel!

    //MARK:- INITIALIZATION
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- SETUP VIEWS AND CONSTRAINTS
    func setupViews() {
        contentView.backgroundColor = Theme.sharedInstance.getBackgroundColor()
        selectionStyle = .none
        
        idealPlaceLabel = Utilities.sharedInstance.createLabelWith(text: Constants.Venue.Strings.kIdealPlace, txtAlignment: .left, font: .systemFont(ofSize: 15, weight: .semibold), textColor: .gray, backgroundColor: .clear)
        venueDescLabel = Utilities.sharedInstance.createLabelWith(text: "", txtAlignment: .left, font: .systemFont(ofSize: 13, weight: .light), textColor: .black, backgroundColor: .clear)
        
        contentView.addSubview(idealPlaceLabel)
        contentView.addSubview(venueDescLabel)
    }
    
    func setupConstraints() {
        idealPlaceLabel.snp.makeConstraints { (make) in
            make.left.equalTo(contentView).offset(20)
            make.right.equalTo(contentView).offset(-20)
            make.top.equalTo(contentView).offset(10)
        }
        
        venueDescLabel.snp.makeConstraints { (make) in
            make.left.right.equalTo(idealPlaceLabel)
            make.top.equalTo(idealPlaceLabel.snp.bottom).offset(20)
            make.bottom.equalTo(contentView).offset(-15)
        }
    }
    
    //MARK: - SETUP CELL
    func setupCellWith(venue: Venue) {
        if let venueDesc = venue.description {
            var helperDesc = ""
            helperDesc = venueDesc.replacingOccurrences(of: "<br />", with: "\n")
            venueDescLabel.text = helperDesc
        }
    }
}
