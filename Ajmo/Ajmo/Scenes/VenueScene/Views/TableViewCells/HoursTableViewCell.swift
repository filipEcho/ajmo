//
//  HoursTableViewCell.swift
//  Ajmo
//
//  Created by Filip Stevanoski on 10/8/20.
//  Copyright © 2020 Filip Stevanoski. All rights reserved.
//

import UIKit

class HoursTableViewCell: UITableViewCell {
    
    //Properties
    var dayLabel: UILabel!
    var hoursLabel: UILabel!

    //MARK:- INITIALIZATION
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- SETUP VIEWS AND CONSTRAINTS
    func setupViews() {
        contentView.backgroundColor = .clear
        selectionStyle = .none
        
        dayLabel = Utilities.sharedInstance.createLabelWith(text: "", txtAlignment: .left, font: .systemFont(ofSize: 14, weight: .semibold), textColor: .gray, backgroundColor: .clear)
        hoursLabel = Utilities.sharedInstance.createLabelWith(text: "", txtAlignment: .left, font: .systemFont(ofSize: 12, weight: .regular), textColor: .black, backgroundColor: .clear)

        contentView.addSubview(dayLabel)
        contentView.addSubview(hoursLabel)
    }
    
    func setupConstraints() {
        hoursLabel.snp.makeConstraints { (make) in
            make.left.equalTo(contentView.snp.centerX).offset(-20)
            make.centerY.equalTo(contentView.snp.centerY)
        }
        
        dayLabel.snp.makeConstraints { (make) in
            make.right.equalTo(hoursLabel.snp.left).offset(-10)
            make.centerY.equalTo(contentView.snp.centerY)
        }
    }

    //MARK: - SETUP CELL
    func setupCellWith(workingHours: WorkingHours) {
        if let day = workingHours.day {
            dayLabel.text = getTheDay(day:day)
        }
        
        if let startHour = workingHours.start {
            if let endhour = workingHours.end {
                hoursLabel.text = startHour + " - " + endhour
            }
        }
    }
    
    //MARK: - SETUP CELL
    //Get the day string
    func getTheDay(day: Int) -> String {
        switch day {
        case 0:
            return Constants.Venue.Strings.Days.kMOn
        case 1:
            return Constants.Venue.Strings.Days.kTue
        case 2:
            return Constants.Venue.Strings.Days.kWed
        case 3:
            return Constants.Venue.Strings.Days.kThu
        case 4:
            return Constants.Venue.Strings.Days.kFri
        case 5:
            return Constants.Venue.Strings.Days.KSat
        case 6:
            return Constants.Venue.Strings.Days.kSun
        default:
            return ""
        }
    }
}
