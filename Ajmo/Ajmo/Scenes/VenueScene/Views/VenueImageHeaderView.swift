//
//  VenueImageHeaderView.swift
//  Ajmo
//
//  Created by Filip Stevanoski on 10/8/20.
//  Copyright © 2020 Filip Stevanoski. All rights reserved.
//

import UIKit

class VenueImageHeaderView: UIView {

    var venueImageView: UIImageView!
    
    //MARK: - INITIALIZATION
    override init(frame : CGRect) {
        super.init(frame: frame)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - SETUP VIEWS AND CONSTRAINTS
    func setupViews() {
        self.backgroundColor = .white
        
        venueImageView = Utilities.sharedInstance.createImageViewWith(imageName: "", backgroundColor: .white, contentMode: .scaleToFill, corner: 0.0)
        
        self.addSubview(venueImageView)
    }
    
    func setupConstraints() {
        venueImageView.snp.makeConstraints { (make) in
            make.edges.equalTo(self)
        }
    }
    
    //MARK: - SETUP VIEW WITH IMAGE
    func setupViewWith(imageName: String) {
        if let imageUrl = URL(string: imageName) {
            venueImageView.kf.setImage(with: imageUrl)
        }
    }
}
