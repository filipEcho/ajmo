//
//  VenueViewController.swift
//  Ajmo
//
//  Created by Filip Stevanoski on 10/8/20.
//  Copyright © 2020 Filip Stevanoski. All rights reserved.
//

import UIKit
import MapKit

class VenueViewController: UIViewController {
    
    //Properties
    var tableView: UITableView!
    var openHoursView: OpenHoursView!
    var venue: Venue!
    var galleryImages = [Gallery]()
    var isVenueOpen = false
    
    //MARK: - INITIALIZATION
    init(selectedVenue: Venue) {
        super.init(nibName: nil, bundle: nil)
        self.venue = selectedVenue
    }
        
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    //MARK: - CONTROLLER LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupConstraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = ""
        self.navigationItem.largeTitleDisplayMode = .never
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    //MARK: - SETUP VIEWS AND CONSTRAINTS
    func setupViews() {
        self.view.backgroundColor = Theme.sharedInstance.getBackgroundColor()
        self.edgesForExtendedLayout = []
        
        //Check if there are gallery images
        let headerView = StretchyTableHeaderView()
        if venue.gallery?.count ?? 0 > 0 {
            headerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height/3.2)
            headerView.setupViewWith(imageName: venue.gallery?.first?.picture ?? "")
        }
        
        galleryImages = venue.gallery ?? [Gallery]()
        
        tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.tableHeaderView = headerView
        tableView.tableFooterView = UIView()
        tableView.estimatedRowHeight = 100.0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(VenueNameTableViewCell.self, forCellReuseIdentifier: Constants.Venue.Cells.kVenueNameCell)
        tableView.register(VenueInfoTableViewCell.self, forCellReuseIdentifier: Constants.Venue.Cells.kVenueInfoCell)
        tableView.register(SmokingTableViewCell.self, forCellReuseIdentifier: Constants.Venue.Cells.kSmokingCell)
        tableView.register(VenueDescriptionTableViewCell.self, forCellReuseIdentifier: Constants.Venue.Cells.kDescrtiptionCell)
        tableView.register(GalleryTableViewCell.self, forCellReuseIdentifier: Constants.Venue.Cells.kGalleryCell)
        tableView.register(VenueTagsTableViewCell.self, forCellReuseIdentifier: Constants.Venue.Cells.kVenuePrimaryTagsCell)
        tableView.register(SecondaryTagsTableViewCell.self, forCellReuseIdentifier: Constants.Venue.Cells.kVenueSecondaryTagsCell)
        tableView.showsVerticalScrollIndicator = true
        
        openHoursView = OpenHoursView(frame: self.view.frame, workingHours: venue.working_hours ?? [WorkingHours]())
        openHoursView.isHidden = true
        openHoursView.delegate = self
        
        view.addSubview(tableView)
        view.addSubview(openHoursView)
    }
    
    func setupConstraints() {
        tableView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        
        openHoursView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
    }
}

//MARK: - UITABLEVIEW DELEGATE AND DATASOURCE
extension VenueViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0,2,3,4,5,6:
            return 1
        case 1:
            return numberOfInfoCells()
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0, 3:
            return UITableView.automaticDimension
        case 1:
            return 45.0
        case 2:
            return self.view.frame.size.height/8
        case 4:
            if galleryImages.count > 0 {
                if Utilities.sharedInstance.iphoneType(type: .IPhoneXorBigger) {
                    return self.view.frame.size.height/4
                } else {
                    return self.view.frame.size.height/3.8
                }
            }
            return 0.0
            
        case 5:
            if venue.allTags?.primaryTags?.count ?? 0 > 0 {
                if Utilities.sharedInstance.iphoneType(type: .IPhoneXorBigger) {
                    return self.view.frame.size.height/6
                } else {
                    return self.view.frame.size.height/5.6
                }
            }
            return 0.0
        case 6:
            if venue.allTags?.secondaryTags?.count ?? 0 > 0 {
                return self.view.frame.size.height/3
            }
            return 0.0
        default:
            return 0.0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Venue.Cells.kVenueNameCell, for: indexPath) as! VenueNameTableViewCell
            cell.setupCellWith(venue: venue)
            
            return cell
            
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Venue.Cells.kVenueInfoCell, for: indexPath) as! VenueInfoTableViewCell
            cell.setupCellWith(venue: venue, index: indexPath.row)
            
            return cell
            
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Venue.Cells.kSmokingCell, for: indexPath) as! SmokingTableViewCell
            cell.setupCellWith(venue: venue)
            
            return cell
            
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Venue.Cells.kDescrtiptionCell, for: indexPath) as! VenueDescriptionTableViewCell
            cell.setupCellWith(venue: venue)
            
            return cell
            
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Venue.Cells.kGalleryCell, for: indexPath) as! GalleryTableViewCell
            
            if galleryImages.count > 0 {
                cell.setupCellWith(images: galleryImages)
            }
            cell.delegate = self
            
            return cell
            
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Venue.Cells.kVenuePrimaryTagsCell, for: indexPath) as! VenueTagsTableViewCell
            cell.setupCellWith(venue: venue)
            cell.delegate = self
            
            return cell
            
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Venue.Cells.kVenueSecondaryTagsCell, for: indexPath) as! SecondaryTagsTableViewCell
            cell.setupCellWith(venue: venue)
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 1:
            if indexPath.row == 0 {
                self.openHoursView.alpha = 1.0
                self.openHoursView.isHidden = false
            } else {
                if indexPath.row == 1 {
                    if venue.address != nil {
                        self.openMapForPlace()
                    } else if venue.telephone != nil {
                        self.callVenue()
                    }
                } else {
                    self.callVenue()
                }
            }
        default:
            return
        }
    }
}

//MARK: - HELPERS
extension VenueViewController {
    func numberOfInfoCells() -> Int {
        var infoCount = 0
        if venue.working_hours?.count ?? 0 > 0 {
            infoCount += 1
        }
        
        if venue.address != nil && venue.address != "" {
            infoCount += 1
        }
        
        if venue.telephone != nil && venue.telephone != "" {
            infoCount += 1
        }
        
        return infoCount
    }
    
    func openMapForPlace() {
        if let lat = venue.lat {
            if let lon = venue.lon {
                let regionDistance:CLLocationDistance = 10000
                let coordinates = CLLocationCoordinate2DMake(lat, lon)
                let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
                let options = [
                    MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
                    MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
                ]
                let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
                let mapItem = MKMapItem(placemark: placemark)
                if let venueName = venue.name {
                    mapItem.name = venueName
                }
                mapItem.openInMaps(launchOptions: options)
            }
        }
    }
    
    func callVenue() {
        if let phoNumber = venue.telephone {
            let helperPhone = phoNumber.replacingOccurrences(of: " ", with: "")
            if let url = URL(string: "tel://" + helperPhone) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
    }
}

//MARK: - OPEN HOURS VIEW DELEGATE
extension VenueViewController: OpenHoursViewDelegate {
    func okButtonAction() {
        UIView.animate(withDuration: 0.3, animations: {
            self.openHoursView.alpha = 0
        }) { (finished) in
            self.openHoursView.isHidden = finished
        }
    }
}

//MARK: - VENUE TAGS CELL DELEGATE
extension VenueViewController: VenueTagsTableViewCellDelegate {
    func selectedTag(tag: Tag) {
        self.navigationController?.pushViewController(FilteredVenuesViewController(filteredVenues: Utilities.sharedInstance.filterVenuesByPrimaryTag(tag: tag, venues: DataModel.sharedInstace.venues), tag: tag), animated: true)
    }
}

//MARK: - GALLERY TABLEVIEW CELL DELEGATE
extension VenueViewController: GalleryTableViewCellDelegate {
    func imageSelectedAtIndex(index: Int) {
        if galleryImages.count > 3 {
            self.presentGalleryVC(indexOfFirstImage: index + 1)
        } else {
            self.presentGalleryVC(indexOfFirstImage: index)
        }
    }
    
    func presentGalleryVC(indexOfFirstImage: Int) {
        if galleryImages.count > 0 {
            let galeryNav = UINavigationController(rootViewController: GalleryViewController(images: galleryImages, indexOfFirstImage: indexOfFirstImage))
            self.present(galeryNav, animated: false, completion: nil)
        }
    }
}

//MARK: - SCROLLVIEW DELEGATE
extension VenueViewController : UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let headerView = self.tableView.tableHeaderView as! StretchyTableHeaderView
        headerView.scrollViewDidScroll(scrollView: scrollView)
    }
}


