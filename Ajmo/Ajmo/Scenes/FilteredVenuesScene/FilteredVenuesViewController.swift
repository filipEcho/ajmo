//
//  FilteredVenuesViewController.swift
//  Ajmo
//
//  Created by Filip Stevanoski on 10/8/20.
//  Copyright © 2020 Filip Stevanoski. All rights reserved.
//

import UIKit

class FilteredVenuesViewController: UIViewController {
    
    var venuesTableView: UITableView!
    var noDataLabel: UILabel!
    var venues = [Venue]()
    var tag: Tag!
    
    //MARK: - INITIALIZATION
    init(filteredVenues: [Venue], tag: Tag?) {
        super.init(nibName: nil, bundle: nil)
        self.venues = Utilities.sharedInstance.sortVenuesAlphabetically(venues: filteredVenues)
        self.tag = tag
    }
        
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    //MARK: - CONTROLLER LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupConstraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = ""
        self.navigationItem.largeTitleDisplayMode = .never
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    func setupViews() {
        self.navigationController?.navigationBar.prefersLargeTitles = false
        
        self.view.backgroundColor = Theme.sharedInstance.getBackgroundColor()
        self.edgesForExtendedLayout = []
        
        //TableView
        venuesTableView = UITableView()
        venuesTableView.delegate = self
        venuesTableView.dataSource = self
        venuesTableView.backgroundColor = .clear
        venuesTableView.tableFooterView = UIView()
        venuesTableView.register(VenueTableViewCell.self, forCellReuseIdentifier: Constants.FilteredVenues.Cells.kAllVenuesCell)
        venuesTableView.showsVerticalScrollIndicator = true
        
        noDataLabel = Utilities.sharedInstance.createLabelWith(text: Constants.FilteredVenues.kNoVenues, txtAlignment: .center, font: .systemFont(ofSize: 20, weight: .semibold), textColor: .black, backgroundColor: .clear)
        
        if venues.count > 0 {
            noDataLabel.isHidden = true
            venuesTableView.isHidden = false
        } else {
            venuesTableView.isHidden = true
            noDataLabel.isHidden = false
        }
        
        view.addSubview(venuesTableView)
        view.addSubview(noDataLabel)
    }
    
    func setupConstraints() {
        venuesTableView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        
        noDataLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.view.snp.centerY)
            make.left.right.equalTo(self.view)
        }
    }
}

//MARK: - TABLEVIEW DELEGATE AND DATASOURCE
extension FilteredVenuesViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return venues.count
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if Utilities.sharedInstance.iphoneType(type: .IPhoneXorBigger) {
            return self.view.frame.size.height/6.5
        } else {
            return self.view.frame.size.height/5.5
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.FilteredVenues.Cells.kAllVenuesCell, for: indexPath) as! VenueTableViewCell
        cell.setupCellWith(venue: venues[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = Theme.sharedInstance.getBackgroundColor()
        var sectionTitleLabel: UILabel!
        
        sectionTitleLabel = Utilities.sharedInstance.createLabelWith(text: Constants.FilteredVenues.kFilters + "\(tag.name ?? "")", txtAlignment: .left, font: .systemFont(ofSize: 15, weight: .regular), textColor: .secondaryLabel, backgroundColor: .clear)
        
        view.addSubview(sectionTitleLabel)
        
        let bottomView = Utilities.sharedInstance.createView(cornerRadius: 0.0, backgorundColor: .secondaryLabel)
        view.addSubview(bottomView)
        
        sectionTitleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(view).offset(20)
            make.centerY.equalTo(view.snp.centerY)
        }
        
        bottomView.snp.makeConstraints { (make) in
            make.left.right.equalTo(view)
            make.bottom.equalTo(view)
            make.height.equalTo(0.5)
        }
        
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return self.view.frame.size.height/14
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.navigationController?.pushViewController(VenueViewController(selectedVenue: venues[indexPath.row]), animated: true)
    }
}
