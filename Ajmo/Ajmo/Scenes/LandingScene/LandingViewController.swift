//
//  LandingViewController.swift
//  Ajmo
//
//  Created by Filip Stevanoski on 10/7/20.
//  Copyright © 2020 Filip Stevanoski. All rights reserved.
//

import UIKit
import SnapKit

class LandingViewController: UIViewController {
    
    //UI Parameters
    var venuesCollectionView: UICollectionView!
    var titleLabel: UILabel!
    var activityIndicator: UIActivityIndicatorView!
    
    //Helpers
    var venueCategories = [VenueCategory]()
    var venues = [Venue]()
    
    //MARK: - CONTROLLER LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupViews()
        setupConstraints()
        getVenues()
    }
    
    //MARK: - SETUP NAVIGATION BAR
    func setupNavigationBar() {
        self.setNavigationBar(title: Constants.Landing.kDiscover, largeTitle: true,displayModeAlways: true)
        self.navigationItem.largeTitleDisplayMode = .always
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    //MARK: - SETUP VIEWS AND CONSTRAINTS
    func setupViews() {
        self.view.backgroundColor = Theme.sharedInstance.getBackgroundColor()
        self.edgesForExtendedLayout = []
        
        titleLabel = Utilities.sharedInstance.createLabelWith(text: Constants.Landing.kVenues, txtAlignment: .left, font: .systemFont(ofSize: 22, weight: .semibold), textColor: .black, backgroundColor: .clear)
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        
        venuesCollectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        venuesCollectionView.backgroundColor = .clear
        venuesCollectionView.register(VenueCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        venuesCollectionView.showsHorizontalScrollIndicator = false
        venuesCollectionView.delegate = self
        venuesCollectionView.dataSource = self
        venuesCollectionView.isHidden = true
        
        self.activityIndicator = UIActivityIndicatorView()
        self.activityIndicator.color = .black
        self.activityIndicator.hidesWhenStopped = true
        
        self.view.addSubview(self.titleLabel)
        self.view.addSubview(self.venuesCollectionView)
        self.view.addSubview(self.activityIndicator)
    }
    
    func setupConstraints() {
        self.titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.view).offset(20)
            make.top.equalTo(self.view).offset(20)
        }
        
        self.venuesCollectionView.snp.makeConstraints { (make) in
            make.left.equalTo(self.view).offset(15)
            make.right.equalTo(self.view).offset(-15)
            make.top.equalTo(self.titleLabel.snp.bottom).offset(5)
            if Utilities.sharedInstance.iphoneType(type: .IPhoneXorBigger) {
                make.height.equalTo(self.view.frame.size.height/8)
            } else {
                make.height.equalTo(self.view.frame.size.height/7.5)
            }
        }
        
        self.activityIndicator.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.view.snp.centerX)
            make.centerY.equalTo(self.view.snp.centerY)
            make.height.width.equalTo(self.view.frame.size.width).dividedBy(4)
        }
    }
}

//MARK: - UICOLLECTIONVIEW DELEGATE, DATASOURCE & FLOW LAYOUT
extension LandingViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return venueCategories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! VenueCollectionViewCell
        cell.setupCellWith(venue: venueCategories[indexPath.row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.navigationController?.pushViewController(VenueCategoryViewController(venueCategory: venueCategories[indexPath.row], trendingVenues: Utilities.sharedInstance.filterVenuesByCategory(category: venueCategories[indexPath.row], venues: self.venues)), animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.view.frame.size.width - 30)/3 , height: venuesCollectionView.frame.size.height)
    }
}

//MARK: - API COMMUNICATION
extension LandingViewController {
    func getVenues() {
        if Utilities.sharedInstance.hasInternetConnection() {
            self.activityIndicator.startAnimating()
            ApiManager.sharedInstance.getVenues { (success, response, statusCode) in
                if success {
                    if let statusCode = statusCode {
                        if statusCode == 200 {
                            if let responseJson = response {
                                if let data = responseJson["data"] as? [String: Any] {
                                    
                                    //VenueCategories
                                    if let venuesCategoriesArray = data["venueCategories"] as? [[String: Any]] {
                                        if venuesCategoriesArray.count > 0 {
                                            self.venueCategories.removeAll()
                                            for venueCatDict in venuesCategoriesArray {
                                                let decoder = JSONDecoder()
                                                let json = Utilities.sharedInstance.jsonToData(json: venueCatDict)
                                                do {
                                                    let venueCategory = try decoder.decode(VenueCategory.self, from: json!)
                                                    self.venueCategories.append(venueCategory)
                                                } catch {
                                                    print(error.localizedDescription)
                                                }
                                            }
                                        }
                                    }
                                    
                                    //Venues
                                    if let venues = data["trending"] as? [[String: Any]] {
                                        if venues.count > 0 {
                                            self.venues.removeAll()
                                            for venueDict in venues {
                                                let decoder = JSONDecoder()
                                                let json = Utilities.sharedInstance.jsonToData(json: venueDict)
                                                do {
                                                    let venue = try decoder.decode(Venue.self, from: json!)
                                                    self.venues.append(venue)
                                                } catch {
                                                    print(error.localizedDescription)
                                                }
                                            }
                                        }
                                    }
                                    DataModel.sharedInstace.venues = self.venues
                                    
                                    DispatchQueue.main.async {
                                        self.activityIndicator.stopAnimating()
                                        self.venuesCollectionView.reloadData()
                                        self.venuesCollectionView.isHidden = false
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
