//
//  VenueCollectionViewCell.swift
//  Ajmo
//
//  Created by Filip Stevanoski on 10/7/20.
//  Copyright © 2020 Filip Stevanoski. All rights reserved.
//

import UIKit
import Kingfisher

class VenueCollectionViewCell: UICollectionViewCell {
    
    //Parameters
    var holderView: UIView!
    var venueImageView: UIImageView!
    var venueNameLabel: UILabel!
    
    //MARK: - INITIALIZATION
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - SETUP VIEWS AND CONSTRAINTS
    func setupViews() {
        holderView = Utilities.sharedInstance.createView(cornerRadius: 12.0, backgorundColor: .white)
        holderView.frame = CGRect(x: 0, y: 0, width: self.frame.self.width - 60, height: self.frame.size.height*0.85)
        Utilities.sharedInstance.addShadowTo(view: holderView)
        
        venueImageView = Utilities.sharedInstance.createImageViewWith(imageName: "", backgroundColor: .clear, contentMode: .scaleAspectFit, corner: 0.0)
        holderView.addSubview(self.venueImageView)
        
        venueNameLabel = Utilities.sharedInstance.createLabelWith(text: "", txtAlignment: .center, font: .systemFont(ofSize: 11, weight: .light), textColor: .black, backgroundColor: .clear)
        holderView.addSubview(self.venueNameLabel)
        
        contentView.addSubview(self.holderView)
    }
    
    func setupConstraints() {
        holderView.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.contentView.snp.centerX)
            make.centerY.equalTo(self.contentView.snp.centerY)
            make.height.equalTo(self.contentView.snp.height).multipliedBy(0.85)
            make.left.equalTo(self.contentView).offset(20)
            make.right.equalTo(self.contentView).offset(-20)
        }
        
        venueImageView.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.holderView.snp.centerX)
            make.top.equalTo(self.holderView).offset(9)
            make.height.width.equalTo(self.holderView.snp.height).multipliedBy(0.55)
        }
        
        venueNameLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.holderView).offset(3)
            make.right.equalTo(self.holderView).offset(-3)
            make.top.equalTo(self.venueImageView.snp.bottom).offset(5)
        }
    }
    
    func setupCellWith(venue: VenueCategory) {
        if let image = venue.image {
            if let imageUrl = URL(string: image) {
                venueImageView.kf.setImage(with: imageUrl)
            }
        }
        if let venueCategoryName = venue.name {
            venueNameLabel.text = venueCategoryName
        }
    }
}
