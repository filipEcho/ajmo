//
//  Gallery.swift
//  Ajmo
//
//  Created by Filip Stevanoski on 10/7/20.
//  Copyright © 2020 Filip Stevanoski. All rights reserved.
//

import Foundation

class Gallery: Codable {
    
    var id: Int?
    var venue_id: Int?
    var picture: String?
    var created_at: Int?
    var updated_at: Int?
}
