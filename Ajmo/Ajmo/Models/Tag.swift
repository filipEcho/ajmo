//
//  Tag.swift
//  Ajmo
//
//  Created by Filip Stevanoski on 10/7/20.
//  Copyright © 2020 Filip Stevanoski. All rights reserved.
//

import Foundation

class Tag: Codable {
    
    var id: Int?
    var color: String?
    var tag_group_id: Int?
    var name: String?
    
}
