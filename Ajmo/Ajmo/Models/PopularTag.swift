//
//  PopularTag.swift
//  Ajmo
//
//  Created by Filip Stevanoski on 10/7/20.
//  Copyright © 2020 Filip Stevanoski. All rights reserved.
//

import Foundation

class PopularTag: Codable {
    
    var tag: Tag?
    var tags: Int?
}
