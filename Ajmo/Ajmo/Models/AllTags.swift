//
//  AllTag.swift
//  Ajmo
//
//  Created by Filip Stevanoski on 10/8/20.
//  Copyright © 2020 Filip Stevanoski. All rights reserved.
//

import Foundation

class AllTags: Codable {
    
    var primaryTags: [Tag]?
    var secondaryTags: [Tag]?
}
