//
//  WorkingHours.swift
//  Ajmo
//
//  Created by Filip Stevanoski on 10/7/20.
//  Copyright © 2020 Filip Stevanoski. All rights reserved.
//

import Foundation

class WorkingHours: Codable {
    
    var id: Int?
    var venue_id: Int?
    var day: Int?
    var start: String?
    var end: String?
}
