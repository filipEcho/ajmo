//
//  VenueCategory.swift
//  Ajmo
//
//  Created by Filip Stevanoski on 10/7/20.
//  Copyright © 2020 Filip Stevanoski. All rights reserved.
//

import Foundation

class VenueCategory: Codable {
    
    var id: Int?
    var name: String?
    var image: String?
    var gradient_color_start: String?
    var gradient_color_end: String?
    var feeling_lucky_title: String?
    var popularTags: [PopularTag]?
}
